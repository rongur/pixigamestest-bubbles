﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{

    float explosionRadius;


    void Start()
    {
        GameEvents.current.OnBombClickedOn += OnBombClickedOn;
        explosionRadius = GetComponent<CircleCollider2D>().radius * GetComponent<Transform>().localScale.x;
    }


    private void OnBombClickedOn(Bomb clickedBomb)
    {
        if (clickedBomb.GetInstanceID() == this.GetInstanceID())
        {
            ExplodeBomb();

            Destroy(gameObject);
        }
    }

    private void ExplodeBomb()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(GetComponent<Transform>().position, explosionRadius);
        foreach (Collider2D collider in colliders)
        {
            Bubble bubble = collider.GetComponent<Bubble>();
            if (bubble != null)
            {
                bubble.PopBubble();
            }
        }
    }

    private void OnDestroy()
    {
        GameEvents.current.OnBombClickedOn -= OnBombClickedOn;
    }
}
