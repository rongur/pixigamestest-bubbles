﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smasher : MonoBehaviour
{

    void Start()
    {
        GameEvents.current.OnSmasherClickedOn += OnSmasherClickedOn;
    }

    private void OnSmasherClickedOn(Smasher clickedSmasher)
    {

        if (clickedSmasher.GetInstanceID() == this.GetInstanceID())
        {
            Destroy(this.gameObject);
        }

    }


    private void OnDestroy()
    {
        GameEvents.current.OnSmasherClickedOn -= OnSmasherClickedOn;
    }
}
