﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    [SerializeField] private int playerHealth;
    public int PlayerHealth { get { return playerHealth; } }

    [SerializeField] private float healthDecreaseRate;
    private float timeToNextHealthDecrease;
    private float timeToNextRateIncrease;

    [SerializeField] private GameSettings gameSettings;




    private void Start()
    {
        playerHealth = gameSettings.MaxHealth;
        healthDecreaseRate = gameSettings.HealthInitialDecreaseRate;
        timeToNextHealthDecrease = 0;
        timeToNextRateIncrease = 0;
       timeToNextRateIncrease = gameSettings.HealthDecreaseInterval;
        GameEvents.current.OnAidKitClickedOn += OnAidKitClickedOn;
    }


    private void Update()
    {
        if (Time.timeSinceLevelLoad > timeToNextHealthDecrease)
        {
            timeToNextHealthDecrease += healthDecreaseRate;

            DecreaseHealth();

        }

        if (Time.timeSinceLevelLoad > timeToNextRateIncrease)
        {
            timeToNextRateIncrease += gameSettings.HealthDecreaseInterval;

            //Increase Rate
            healthDecreaseRate -= healthDecreaseRate*0.2f;
        }

    }

    private void DecreaseHealth()
    {
        playerHealth = Mathf.Clamp(playerHealth - gameSettings.HealthDecreaseVal, 0, gameSettings.MaxHealth);

        GameEvents.current.HealthChanged(playerHealth);
    }

    private void OnAidKitClickedOn(AidKit aidKit)
    {
        playerHealth += gameSettings.AidKitHealth;
    }

    private void OnDestroy()
    {
        GameEvents.current.OnAidKitClickedOn -= OnAidKitClickedOn;
    }




}
