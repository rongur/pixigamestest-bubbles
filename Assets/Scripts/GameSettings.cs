﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameSettings")]
public class GameSettings : ScriptableObject
{

    //Bubble settings
    [SerializeField] private int bubblesStartSize;
    public int BubblesStartSize { get { return bubblesStartSize; } }

    [SerializeField] private int bubblePopScore;
    public int BubblePopScore { get { return bubblePopScore; } }

    [SerializeField] private int popsToCombo;
    public int PopsToCombo { get { return popsToCombo; } }


    //Health settings
    [SerializeField] private int maxHealth;
    public int MaxHealth { get { return maxHealth; } }

    [SerializeField] private int healthInitialDecreaseRate;
    public int HealthInitialDecreaseRate { get { return healthInitialDecreaseRate; } }

    [SerializeField] private int healthDecreaseVal;
    public int HealthDecreaseVal { get { return healthDecreaseVal; } }

    [SerializeField] private int healthDecreaseInterval;
    public int HealthDecreaseInterval { get { return healthDecreaseInterval; } }

    [SerializeField] private int healthDecreaseMultiplyer;
    public int HealthDecreaseMultiplyer { get { return healthDecreaseMultiplyer; } }



    //PowerUp settings
    [SerializeField] private int powerUpProb;
    public int PowerUpProb { get { return powerUpProb; } }

    [SerializeField] private int bombProb;
    public int BombProb { get { return bombProb; } }

    [SerializeField] private int aidKitProb;
    public int AidKitProb { get { return aidKitProb; } }

    [SerializeField] private int aidKitHealth;
    public int AidKitHealth { get { return aidKitHealth; } }

    [SerializeField] private int smasherProb;
    public int SmasherProb { get { return smasherProb; } }




}
