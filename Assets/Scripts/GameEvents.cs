﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    private void Awake()
    {
        current = this;
    }

    public event Action<Bubble> OnBubbleClickedOn;
    public event Action<AidKit> OnAidKitClickedOn;
    public event Action<Bomb> OnBombClickedOn;
    public event Action<Smasher> OnSmasherClickedOn;
    public event Action<Bubble> OnBubblePopped;
    public event Action<int> OnHealthChanged;


    public void BubbleClickedOn(Bubble bubble)
    {
        if (OnBubbleClickedOn != null)
        {
            OnBubbleClickedOn(bubble);
        }
    }

    public void AidKitClickedOn(AidKit aidkit)
    {
        if (OnAidKitClickedOn != null)
        {
            OnAidKitClickedOn(aidkit);
        }
    }

    public void BombClickedOn(Bomb bomb)
    {
        if (OnBombClickedOn != null)
        {
            OnBombClickedOn(bomb);
        }
    }
    public void SmasherClickedOn(Smasher smasher)
    {
        if (OnSmasherClickedOn != null)
        {
            OnSmasherClickedOn(smasher);
        }
    }


    public void BubblePopped(Bubble bubble)
    {
        if (OnBubblePopped != null)
        {
            OnBubblePopped(bubble);
        }
    }

    public void HealthChanged(int currenHealth)
    {
        if (OnHealthChanged != null)
        {
            OnHealthChanged(currenHealth);
        }
    }
}
