﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AidKit : MonoBehaviour
{

    void Start()
    {
        GameEvents.current.OnAidKitClickedOn += OnAidKitClickedOn;
    }


    private void OnAidKitClickedOn(AidKit clickedAidKit)
    {
        if (clickedAidKit.GetInstanceID() == this.GetInstanceID())
        {
            Destroy(this.gameObject);
        }
    }


    private void OnDestroy()
    {
        GameEvents.current.OnAidKitClickedOn -= OnAidKitClickedOn;
    }
}
