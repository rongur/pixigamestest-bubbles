﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleSpawner : MonoBehaviour
{
    [SerializeField] private Bubble[] bubbles;
    [SerializeField] private GameSettings gameSettings;

    public void Start()
    {
        GameEvents.current.OnBubbleClickedOn += OnBubbleClickedOn;

        for (int i = 0; i < gameSettings.BubblesStartSize; i++)
        {
            int randomBubbleIndex = Random.Range(0, bubbles.Length);
            Bubble bubble = Instantiate(bubbles[randomBubbleIndex], GetComponent<Transform>().position, Quaternion.identity);
        }

    }


    private void OnBubbleClickedOn(Bubble poppedBubble)
    {
        //spawn a new random bubble
        int randomBubbleIndex = Random.Range(0, bubbles.Length);
        Bubble bubble = Instantiate(bubbles[randomBubbleIndex], GetComponent<Transform>().position, Quaternion.identity);
    }

    private void OnDestroy()
    {
        GameEvents.current.OnBubbleClickedOn -= OnBubbleClickedOn;
    }
}
