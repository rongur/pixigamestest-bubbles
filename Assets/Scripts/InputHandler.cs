﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{


    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                Bubble touchedBubble = hit.collider.GetComponent<Bubble>();
                AidKit touchedAidKit = hit.collider.GetComponent<AidKit>();
                Bomb touchedBomb = hit.collider.GetComponent<Bomb>();
                Smasher touchedSmasher = hit.collider.GetComponent<Smasher>();

                if (touchedAidKit != null)
                {
                    GameEvents.current.AidKitClickedOn(touchedAidKit);
                }

                else if (touchedBomb != null)
                {
                    GameEvents.current.BombClickedOn(touchedBomb);
                }


                else if (touchedSmasher != null)
                {
                    GameEvents.current.SmasherClickedOn(touchedSmasher);
                }

                else if (touchedBubble != null)
                {
                    GameEvents.current.BubbleClickedOn(touchedBubble);
                }



            }
        }
    }
}
