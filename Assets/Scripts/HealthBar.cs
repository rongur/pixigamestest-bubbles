﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings;

    //fields for size-change process
    private float maxPosition_x;
    private float minPosition_x;
    private float maxScale_x;
    private float minScale_x;
    private float newPostion_x;
    private float newScale_x;

    private Transform healthBarTransform;



    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.OnHealthChanged += OnHealthChanged;
        healthBarTransform = GetComponent<Transform>();

        
        maxPosition_x = healthBarTransform.position.x;
        minPosition_x = -2.6375f;
        maxScale_x = healthBarTransform.localScale.x;
        minScale_x = 0;

    }

    private void OnHealthChanged(int currentHealth)
    {
        ChangeHealthBarSize(currentHealth);
    }

    private void ChangeHealthBarSize(int currentHealth)
    {
        float progress = ((float)currentHealth / gameSettings.MaxHealth);
        newPostion_x = minPosition_x + progress * (maxPosition_x - minPosition_x);
        newScale_x = minScale_x + progress * (maxScale_x - minScale_x);
        healthBarTransform.position = new Vector3(newPostion_x, healthBarTransform.position.y, healthBarTransform.position.z);
        healthBarTransform.localScale = new Vector3(newScale_x, healthBarTransform.localScale.y, healthBarTransform.localScale.z);
    }

    private void OnDestroy()
    {
        GameEvents.current.OnHealthChanged -= OnHealthChanged;
    }


}
