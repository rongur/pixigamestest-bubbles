﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{

    [SerializeField] private TMPro.TextMeshProUGUI score;
    ScoreManager scoreManager;
    // Start is called before the first frame update
    void Start()
    {

        scoreManager = FindObjectOfType<ScoreManager>();
    }


    private void Update()
    {
        score.text = "Score : " + scoreManager.GameScore.ToString();
    }



}
