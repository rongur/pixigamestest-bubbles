﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour
{
    [SerializeField] private int bubbleType;
    public int BubbleType { get { return bubbleType; } }
    private List<Bubble> sameBubbleTypeNeigbors = new List<Bubble>();
    private GameplayManager gameplayManager;







    public void Start()
    {
        GameEvents.current.OnBubbleClickedOn += OnBubbleClickedOn;
        gameplayManager = FindObjectOfType<GameplayManager>();
    }

    private void OnBubbleClickedOn(Bubble clickedBubble)
    {
        if (clickedBubble.GetInstanceID() == this.GetInstanceID())
        {
            PopBubble();
        }
    }


    public void PopBubble()
    {
        GameEvents.current.BubblePopped(this);

        if (gameplayManager.IsSmasherModeOn)
        {
            PopNeigbors();
            gameplayManager.ResetSmasher();
        }

        Destroy(gameObject);
    }


    private void PopNeigbors()
    {
        for (int i = sameBubbleTypeNeigbors.Count - 1; i >= 0; i--)
        {
            GameEvents.current.BubblePopped(sameBubbleTypeNeigbors[i]);
            Destroy(sameBubbleTypeNeigbors[i].gameObject);

        }
    }

    public int GetBubbleType()
    {
        return bubbleType;
    }


    private void OnDestroy()
    {
        GameEvents.current.OnBubbleClickedOn -= OnBubbleClickedOn;
    }


    //following methods are to keep track of neighbors 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Bubble bubble = collision.gameObject.GetComponent<Bubble>();
        if (bubble != null && bubble.bubbleType == this.bubbleType)
        {
            sameBubbleTypeNeigbors.Add(bubble);
        }


    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        Bubble bubble = collision.gameObject.GetComponent<Bubble>();
        if (bubble != null)
        {
            sameBubbleTypeNeigbors.Remove(bubble);
        }

    }



}
