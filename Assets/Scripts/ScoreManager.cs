﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private int gameScore;
    public int GameScore { get { return gameScore; } }
    private int comboLength;
    private int lastBubblePoppedType;
    [SerializeField] private GameSettings gameSettings;

    private void Start()
    {
        gameScore = 0;
        comboLength = 1;
        lastBubblePoppedType = -1;
        GameEvents.current.OnBubblePopped += OnBubblePopped;

    }


    private void OnBubblePopped(Bubble poppedBubble)
    {
        gameScore += gameSettings.BubblePopScore;

        CheckCombo(poppedBubble);

        lastBubblePoppedType = poppedBubble.GetBubbleType();

    }

    private void CheckCombo(Bubble poppedBubble)
    {
        if (poppedBubble.GetBubbleType() == lastBubblePoppedType)
        {

            comboLength++;
        }
        else
        {
            comboLength = 1;
        }

        if (comboLength > gameSettings.PopsToCombo)
        {
            gameScore += 2 * comboLength;
        }
    }


    private void OnDestroy()
    {
        GameEvents.current.OnBubblePopped -= OnBubblePopped;
    }
}
