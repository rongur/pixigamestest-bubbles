﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour
{
    [SerializeField] private Bomb bomb;
    [SerializeField] private AidKit aidKit;
    [SerializeField] private Smasher smasher;
    [SerializeField] private GameSettings gameSettings;

    public void Start()
    {
        GameEvents.current.OnBubbleClickedOn += OnBubbleClickedOn;

    }

    private void OnBubbleClickedOn(Bubble clickedBubble)
    {
        if (Random.Range(0, 100) < gameSettings.PowerUpProb)
        {
            SpawnPowerUp(clickedBubble.GetComponent<Transform>().position);
        }

    }

    private void SpawnPowerUp(Vector3 position)
    {
        //update spawn position so powerups will appear in front of the bubbles
       Vector3 newPosition = new Vector3(position.x, position.y, -0.01f);

        float chance = Random.Range(1, 100);

        if (chance <= gameSettings.BombProb)
        {
            Bomb newBomb = Instantiate(bomb, newPosition, Quaternion.identity);
        }
        else if (chance <= gameSettings.AidKitProb + gameSettings.BombProb)
        {
            AidKit newAidKit = Instantiate(aidKit, newPosition, Quaternion.identity);
        }
        else
        {
            Smasher newSmasher = Instantiate(smasher, newPosition, Quaternion.identity);
        }
    }

 

    private void OnDestroy()
    {
        GameEvents.current.OnBubbleClickedOn -= OnBubbleClickedOn;
    }
}
