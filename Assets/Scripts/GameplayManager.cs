﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    private bool isSmasherModeOn;
    public bool IsSmasherModeOn { get { return isSmasherModeOn; } }


    // Start is called before the first frame update
    void Start()
    {
        isSmasherModeOn = false;
        GameEvents.current.OnSmasherClickedOn += OnSmasherClickedOn;
        GameEvents.current.OnHealthChanged += OnHealthChanged;

    }

    private void OnSmasherClickedOn(Smasher smasher)
    {

        isSmasherModeOn = true;

    }

    private void OnHealthChanged(int health)
    {
        if (health == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }


    public void ResetSmasher()
    {
        isSmasherModeOn = false;
    }



    private void OnDestroy()
    {
        GameEvents.current.OnSmasherClickedOn -= OnSmasherClickedOn;
        GameEvents.current.OnHealthChanged -= OnHealthChanged;

    }
}
